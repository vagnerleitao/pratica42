
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.FiguraComEixos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagner
 */
public class Pratica42 {
    private static double valorArea;
    private static double valorPerimetro;
    private static double eixom;
    private static double eixoM;
    
    public static void main(String[] args) {
        String areac = "O valor da area do circulo e : ";
        String perc = "\nO valor do Perimetro do circulo e : ";
        String areae = "O valor da area da elipse e : ";
        String pere = "\nO valor do Perimetro da elipse e : ";

        Elipse fig= new Elipse(1,1);
        eixom=fig.getEixoMenor();
        eixoM=fig.getEixoMaior();
        Elipse fig1 = new Elipse(eixom,eixoM);
        valorArea=fig1.getArea();
        valorPerimetro=fig1.getPerimetro();
        System.out.println(areae+valorArea+pere+valorPerimetro+"\n");

        Circulo fig2 = new Circulo(1);
        eixom=fig2.getEixoMenor();
        //eixoM=fig2.getEixoMaior();
        Circulo fig3 = new Circulo(eixom);
        valorArea=fig3.getArea();
        valorPerimetro=fig3.getPerimetro();
        System.out.println(areac+valorArea+perc+valorPerimetro+"\n");
    }
}
