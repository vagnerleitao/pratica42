/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author vagner
 */
public class Circulo extends Elipse{
    private final double raio;
    
    public Circulo(double r){
        super(r,r);
        raio=r;
    }
    
    @Override
    public double getPerimetro(){
        this.perimetro=2*Math.PI*this.raio;
        super.getPerimetro();
        return perimetro;
    }
    
}
