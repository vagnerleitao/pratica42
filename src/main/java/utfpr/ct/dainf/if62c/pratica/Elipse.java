/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.Serializable;

/**
 *
 * @author vagner
 */
public class Elipse implements FiguraComEixos, Serializable{
    public double eixo1;
    public double eixo2;
    public double area;
    public double perimetro;
    
    public Elipse(double r,double s){
        eixo1=r;
        eixo2=s;
    }
    
    @Override
    public double getArea(){
        area = Math.PI*eixo1*eixo2;
        return area;
    }
    
    @Override
    public double getPerimetro(){
        perimetro=Math.PI*(3*(eixo1+eixo2)-Math.sqrt((3*eixo1+eixo2)*(eixo1+3*eixo2)));
        return perimetro;
    }

    @Override
    public double getEixoMenor() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return 1.04;
    }

    @Override
    public double getEixoMaior() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return 4.53;
    }

    @Override
    public String getNome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

}
